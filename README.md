# recipe-app-api-proxy

NGINX proxy app for our recipe app API

## Usage

### Environment Variables

* `LISTEN_PORT` - Port to lisent on (default: `8000`)
* `APP_HOST` - Hostname of the app forward requests to (default: `app`)
* `APP_PORT` - Port of the app for forward requests to (default: `9000`)
